'use strict';
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
   devServer: {
      disableHostCheck: true,
   },
    mode: 'development',
    entry: [
        './src/app.js'
    ],
   output: {
      path: path.resolve(__dirname, './dist')
   },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.less$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'less-loader'
                ]
            },
            { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000&name=[path][name].[ext]' }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};